package com.ciadnt.estagio2020.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private ImageView image;
    private Button rollButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = findViewById(R.id.dice_image);
        rollButton = findViewById(R.id.roll_button);

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rand = new Random();
                int rad_int = rand.nextInt(  6 ) + 1;
                int draw;
                switch (rad_int) {
                    case 1:
                        draw = R.drawable.dice_1;
                        break;
                    case 2:
                        draw = R.drawable.dice_2;
                        break;
                    case 3:
                        draw = R.drawable.dice_3;
                        break;
                    case 4:
                        draw = R.drawable.dice_4;
                        break;
                    case 5:
                        draw = R.drawable.dice_5;
                        break;
                    case 6:
                        draw = R.drawable.dice_6;
                        break;
                        default:
                            draw = R.drawable.empty_dice;
                }
                image.setImageResource(draw);

            }
        });
//        rollButton.setOnClickListener(
//                Toast.makeText(this, "button clicked", Toast.LENGTH_SHORT).show());

    }
}
